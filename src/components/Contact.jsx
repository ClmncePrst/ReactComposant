import "./Contact.css"

function Contact({nom, image, statutConnexion}) {

    return (
                <div className="Contact"> 
                    <h3>{nom}</h3>
                    <img src={image} alt= {`Photo de ${nom}`} />
                    <span className="pastilleSize"> Statut: {statutConnexion ? <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvP4gZMtvTLyRWIykAzTqb39MROAGyss1g7rScGen1-Q&s" alt= "pastille verte de connexion"/> : <img src="https://www.publicdomainpictures.net/pictures/310000/nahled/red-circle.png" alt= "pastille rouge de déconnnexion" />  } </span>
                </div>
    );

}
export default Contact

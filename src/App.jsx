import React from 'react';
import Contact from './components/Contact';
import './App.css'

function App() {

  return (
    <>
      <h1> Contacts </h1>
      <Contact nom= "Chandler Bing" 
               image= "https://i.pinimg.com/736x/66/b2/c8/66b2c8348cc26e0d69644a7625516da0.jpg" 
               statutConnexion= {true} 
      />
      
      <Contact nom= "Phoebe Buffay" 
               image= "https://i.pinimg.com/474x/be/7e/ef/be7eef4e0603c5e008f526e42d5f0477.jpg" 
               statutConnexion= {false} 
      /> 
      
      <Contact nom= "Monica Geller" 
               image= "https://media.warnerbros.fr/media/media/www.warnerbros.fr/4ae5da80.jpg" 
               statutConnexion= {true} 
      />
      
      <Contact nom= "Ross Geller" 
               image= "https://www.serieously.com/app/uploads/2017/10/RossDur_Featured-min.jpg" 
               statutConnexion= {false} 
      />
      
      <Contact nom= "Rachel Green" 
               image= "https://media.vogue.fr/photos/5da46ee3e189b20008ababe9/master/w_1600%2Cc_limit/GettyImages-594760908.jpg" 
               statutConnexion= {true} 
      />
     
      <Contact nom= "Joey Tribbiani" 
               image= "https://media.vanityfair.fr/photos/60d34f06870eb9c0aa82cc5f/4:3/w_840,h_630,c_limit/vf_slider_friends_joey_7985.jpeg" 
               statutConnexion={false} 
      />       
    </>
  );
};

export default App


